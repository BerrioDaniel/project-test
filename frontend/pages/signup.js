import SignUpContent from "../components/SignUp";
import SignInContent from "../components/SignIn";
import RequestResetContent from "../components/RequestReset";
import styled from "styled-components";

const Columns = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  grid-gap: 20px;
`;

const SignUp = props => (
  <Columns>
    <SignUpContent />
    <SignInContent />
    <RequestResetContent />
  </Columns>
);

export default SignUp;
