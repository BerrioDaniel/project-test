import ResetPassword from "../components/Reset";

const Reset = props => (
  <div>
    <p>Reset your password</p>
    <ResetPassword resetToken={props.query.resetToken} />
  </div>
);

export default Reset;
