import React from "react";
import { Query, Mutation } from "react-apollo";
import gql from "graphql-tag";
import { adopt } from "react-adopt";
import User from "./User";
import CartStyles from "./styles/CartStyles";
import CartItem from "./CartItem";
import Supreme from "./styles/Supreme";
import CloseButton from "./styles/CloseButton";
import SickButton from "./styles/SickButton";
import totalPrice from "../lib/calcTotalPrice";
import formatMoney from "../lib/formatMoney";
import TakeMyMoney from "../components/TakeMyMoney";

const LOCAL_STATE_QUERY = gql`
  query {
    cartOpen @client
  }
`;

const TOGGLE_CART_MUTATION = gql`
  mutation {
    toogleCart @client
  }
`;

const Composed = adopt({
  user: ({ render }) => <User>{render}</User>,
  toogleCart: ({ render }) => (
    <Mutation mutation={TOGGLE_CART_MUTATION}>{render}</Mutation>
  ),
  localState: ({ render }) => <Query query={LOCAL_STATE_QUERY}>{render}</Query>
});

class Cart extends React.Component {
  render() {
    return (
      <Composed>
        {({ user, toogleCart, localState }) => {
          const me = user.data.me;
          if (!me) return null;
          return (
            <CartStyles open={localState.data.cartOpen}>
              <header>
                <CloseButton title="Close" onClick={toogleCart}>
                  X
                </CloseButton>
                <Supreme>{me.name} cart</Supreme>
                <p>
                  You have {me.cart.length} Item{" "}
                  {me.cart.length === 1 ? "" : "s"} in your cart
                </p>
              </header>
              <ul>
                {me.cart.map(el => (
                  <CartItem key={el.id} cartItem={el} />
                ))}
              </ul>
              <footer>
                <p>{formatMoney(totalPrice(me.cart))}</p>
                {me.cart.length && (
                  <TakeMyMoney>
                    <SickButton>Checkout</SickButton>
                  </TakeMyMoney>
                )}
              </footer>
            </CartStyles>
          );
        }}
      </Composed>
    );
  }
}

export default Cart;
export { LOCAL_STATE_QUERY, TOGGLE_CART_MUTATION };
