import { mount } from "enzyme";
import toJson from "enzyme-to-json";
import wait from "waait";
import Cart, { LOCAL_STATE_QUERY } from "../Cart";
import { MockedProvider } from "react-apollo/test-utils";
import { CURRENT_USER_QUERY } from "../User";
import { fakeUser, fakeCartItem } from "../../lib/testUtils";

const mocks = [
  {
    request: {
      query: CURRENT_USER_QUERY
    },
    result: {
      data: {
        me: {
          ...fakeUser(),
          cart: [fakeCartItem()]
        }
      }
    }
  }
];

describe("<Cart/>", () => {
  it("Renders and matches snappy", async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <Cart />
      </MockedProvider>
    );
    await wait();
    wrapper.update();
    expect(toJson(wrapper.find("header"))).toMatchSnapshot();
    expect(wrapper.find("CartItem")).toHaveLength(1);
  });
});
