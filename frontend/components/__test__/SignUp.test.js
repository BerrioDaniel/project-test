import { mount } from "enzyme";
import toJson from "enzyme-to-json";
import wait from "waait";
import SignUp, { SIGNUP_MUTATION } from "../SignUp";
import { CURRENT_USER_QUERY } from "../User";
import { MockedProvider } from "react-apollo/test-utils";
import { fakeUser } from "../../lib/testUtils";
import { ApolloConsumer } from "react-apollo";

function type(wrapper, name, value) {
  wrapper
    .find(`input[name="${name}"]`)
    .simulate("change", { target: { name, value } });
}

const me = fakeUser();
const mocks = [
  //SignUp mock mutation
  {
    request: SIGNUP_MUTATION,
    variables: {
      email: me.email,
      name: me.name,
      password: "DBB"
    },
    result: {
      data: {
        signup: {
          __typename: "User",
          id: "abc123",
          email: me.email,
          name: me.name
        }
      }
    }
  },
  //Current user query mock
  {
    request: { query: CURRENT_USER_QUERY },
    result: { data: { me } }
  }
];

describe("<SignUp/>", () => {
  it("Renders and matches snapshot", async () => {
    const wrapper = mount(
      <MockedProvider>
        <SignUp />
      </MockedProvider>
    );
    expect(toJson(wrapper.find("form"))).toMatchSnapshot();
  });

  it("Calss the mutation properly", async () => {
    let apolloClient;
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <ApolloConsumer>
          {client => {
            apolloClient = client;
            return <SignUp />;
          }}
        </ApolloConsumer>
      </MockedProvider>
    );
    await wait();
    wrapper.update();
    type(wrapper, "name", me.name);
    type(wrapper, "email", me.email);
    type(wrapper, "password", "dbb");
    wrapper.update();
    wrapper.find("form").simulate("submit");
    await wait();
    //Query the user out of the apollo client
    const user = await apolloClient.query({ query: CURRENT_USER_QUERY });
    expect(user.data.me).toMatchObject(me);
  });
});
