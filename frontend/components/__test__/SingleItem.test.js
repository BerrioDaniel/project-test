import { mount } from "enzyme";
import toJson from "enzyme-to-json";
import wait from "waait";
import SingleItem, { SINGLE_ITEM_QUERY } from "../SingleItem";
import { MockedProvider } from "react-apollo/test-utils";
import { fakeItem } from "../../lib/testUtils";

describe("Proffs a single item", () => {
  it("Renders with proper data", async () => {
    const mocks = [
      {
        //When someone makes a request with this query and variable combo
        request: { query: SINGLE_ITEM_QUERY, variables: { id: "123" } },
        result: {
          data: {
            item: fakeItem()
          }
        }
      }
    ];
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <SingleItem id="123" />
      </MockedProvider>
    );

    expect(wrapper.text()).toContain("Loading...");
    await wait();
    wrapper.update();

    expect(toJson(wrapper.find("h2"))).toMatchSnapshot();

    expect(toJson(wrapper.find("img"))).toMatchSnapshot();

    expect(toJson(wrapper.find("p"))).toMatchSnapshot();
  });

  it("Erros with a not found item", async () => {
    const mocks = [
      {
        request: { query: SINGLE_ITEM_QUERY, variables: { id: "123" } },
        result: {
          errors: [{ message: "Items not found" }]
        }
      }
    ];
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <SingleItem id="123" />
      </MockedProvider>
    );
    await wait();
    wrapper.update();
    const item = wrapper.find(`[data-test="graphql-error"]`);
    console.log(item, "MY LITTLE LOG");
    expect(item.text()).toContain("Items not found");
    expect(toJson(item)).toMatchSnapshot();
  });
});
