import { mount } from "enzyme";
import toJson from "enzyme-to-json";
import wait from "waait";
import CreateItem, { CREATE_ITEM_MUTATION } from "../CreateItem";
import { MockedProvider } from "react-apollo/test-utils";
import { fakeItem } from "../../lib/testUtils";
import Router from "next/router";

const dogImage = "https://dog.com/ho";

global.fetch = jest.fn().mockResolvedValue({
  json: () => ({
    secure_url: dogImage,
    eager: [{ secure_url: dogImage }]
  })
});

describe("<CreateItem/>", () => {
  it("Renders and matches", () => {
    const wrapper = mount(
      <MockedProvider>
        <CreateItem />
      </MockedProvider>
    );
    const form = wrapper.find(`form[data-test="form"]`);
    expect(toJson(form)).toMatchSnapshot();
  });

  it("Uploads a fiel when changed", async () => {
    const wrapper = mount(
      <MockedProvider>
        <CreateItem />
      </MockedProvider>
    );
    const input = wrapper.find(`input[type="file"]`);
    input.simulate("change", { target: { files: ["fakedog.jpg"] } });
    await wait();
    const component = wrapper.find("CreateItem").instance();
    expect(component.state.image).toEqual(dogImage);
    expect(component.state.largeImage).toEqual(dogImage);
    expect(global.fetch).toHaveBeenCalled();
    global.fetch.mockReset();
  });

  it("Handles state updating", async () => {
    const wrapper = mount(
      <MockedProvider>
        <CreateItem />
      </MockedProvider>
    );
    wrapper
      .find("#title")
      .simulate("change", { target: { value: "Testing", name: "title" } });

    wrapper.find("#price").simulate("change", {
      target: { value: 50000, name: "price", type: "number" }
    });
    wrapper.find("#description").simulate("change", {
      target: { value: "This is a really nice item", name: "description" }
    });

    expect(wrapper.find("CreateItem").instance().state).toMatchObject({
      title: "Testing",
      price: 50000,
      description: "This is a really nice item"
    });
  });

  it("It creates an item when the form is submited", async () => {
    const item = fakeItem();
    const mocks = [
      {
        request: {
          query: CREATE_ITEM_MUTATION,
          variables: {
            title: item.title,
            description: item.description,
            image: "",
            largeImage: "",
            price: item.price
          }
        },
        result: {
          data: {
            createItem: {
              ...fakeItem,
              id: "abc123",
              __typename: "Item"
            }
          }
        }
      }
    ];

    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <CreateItem />
      </MockedProvider>
    );
    // simulate someone filling out the form
    wrapper
      .find("#title")
      .simulate("change", { target: { value: item.title, name: "title" } });
    wrapper
      .find("#price")
      .simulate("change", {
        target: { value: item.price, name: "price", type: "number" }
      });
    wrapper
      .find("#description")
      .simulate("change", {
        target: { value: item.description, name: "description" }
      });
    // mock the router
    Router.router = { push: jest.fn() };
    wrapper.find("form").simulate("submit");
    await wait(50);
    expect(Router.router.push).toHaveBeenCalled();
    expect(Router.router.push).toHaveBeenCalledWith({
      pathname: "/item",
      query: { id: "abc123" }
    });
  });
});
