import { mount } from "enzyme";
import toJson from "enzyme-to-json";
import wait from "waait";
import RequestReset, { REQUEST_RESET_MUTATION } from "../RequestReset";
import { MockedProvider } from "react-apollo/test-utils";

const mocks = [
  {
    request: {
      query: REQUEST_RESET_MUTATION,
      variables: { email: "berriobarrera@gmail.com" }
    },
    result: {
      data: { requestReset: { message: "success", __typename: "Message" } }
    }
  }
];

describe("<RequestReset/>", () => {
  it("Renders and matches snapshot", () => {
    const wrapper = mount(
      <MockedProvider>
        <RequestReset />
      </MockedProvider>
    );
    const form = wrapper.find('form[data-test="form"]');
    expect(toJson(form)).toMatchSnapshot();
  });

  it("Calls the mutation", async () => {
    const wrapper = (
      <MockedProvider mocks={mocks}>
        <RequestReset />
      </MockedProvider>
    );
    //Simulate typing an email
    wrapper.find("input").simulate("change", {
      target: { name: "email", value: "berriobarrera@gmail.com" }
    });
    wrapper.find("form").simulate("submit");
    await wait();

    wrapper.update();
    expect(wrapper.find("p").text()).toContain(
      "Success! Check your email for reset link"
    );
  });
});
