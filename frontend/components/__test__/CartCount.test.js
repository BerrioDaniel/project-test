import { shallow, mount } from "enzyme";
import toJson from "enzyme-to-json";
import CartCount from "../CartCount";

describe("Cart Count", () => {
  it("renders", () => {
    shallow(<CartCount count={10} />);
  });

  it("Matches the snapshot", () => {
    const wrapper = shallow(<CartCount count={11} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it("Updates via props", () => {
    const wrapper = shallow(<CartCount count={50} />);
    expect(toJson(wrapper)).toMatchSnapshot();
    wrapper.setProps({ count: 10 });
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
