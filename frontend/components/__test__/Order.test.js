import { mount } from "enzyme";
import toJson from "enzyme-to-json";
import wait from "waait";
import Order, { SINGLE_ORDER_QUERY } from "../Order";
import { MockedProvider } from "react-apollo/test-utils";
import { CURRENT_USER_QUERY } from "../User";
import { fakeUser, fakeCartItem, fakeOrder } from "../../lib/testUtils";

const mocks = [
  {
    request: { query: SINGLE_ORDER_QUERY, variables: { id: "ord123" } },
    result: {
      data: {
        order: fakeOrder()
      }
    }
  }
];

describe("<Order/>", () => {
  it("Renders the order", async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <Order id="ord123" />
      </MockedProvider>
    );
    await wait();
    wrapper.update();
    const order = wrapper.find(`div[data-test="order"]`);
    expect(toJson(order)).toMatchSnapshot();
  });
});
