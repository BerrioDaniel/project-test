import React, { Component } from "react";
import PropTypes from "prop-types";
import Title from "./styles/Title";
import ItemsStyles from "./styles/ItemStyles";
import Link from "next/link";
import PriceTag from "./styles/PriceTag";
import DeleteButton from "./DeleteItem";
import formatMoney from "../lib/formatMoney";
import AddToCart from "./AddToCart";

class Item extends Component {
  render() {
    const { item } = this.props;
    return (
      <ItemsStyles>
        {item.image && <img src={item.image} alt={item.title} />}
        <Title>
          <Link href={{ pathname: "/item", query: { id: item.id } }}>
            <a>{item.title}</a>
          </Link>
        </Title>
        <PriceTag>{formatMoney(item.price)}</PriceTag>
        <p>{item.description}</p>
        <div className="buttonList">
          <Link href={{ pathname: "/update", query: { id: item.id } }}>
            <a>Edit</a>
          </Link>

          <AddToCart id={item.id} />
          <DeleteButton id={item.id}>Delete Item</DeleteButton>
        </div>
      </ItemsStyles>
    );
  }
}

Item.propTypes = {
  item: PropTypes.object.isRequired
};

export default Item;
