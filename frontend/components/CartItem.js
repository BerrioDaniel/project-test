import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import RemoveFromCart from "./RemoveFromCart";
import formatMoney from "../lib/formatMoney";

const CartItemStyles = styled.li`
  padding: 1rem 0;
  border-bottom: 1px solid ${props => props.theme.lightGrey};
  display: grid;
  align-items: center;
  grid-template-columns: auto 1fr auto;
  img {
    margin-right: 10px;
  }
  h3,
  p {
    margin: 0;
  }
`;

class CartItem extends React.Component {
  static propTypes = {
    cartItem: PropTypes.object.isRequired
  };

  render() {
    const { cartItem } = this.props;
    return (
      <CartItemStyles>
        <img width="100" src={cartItem.item.image} alt={cartItem.item.title} />
        <div className="cart-item-datails">
          <h3>{cartItem.item.title}</h3>
          <p>
            {formatMoney(cartItem.item.price)} {" - "}
            <em>{cartItem.quantity}</em> {formatMoney(cartItem.item.price)} each
          </p>
        </div>
        <RemoveFromCart id={cartItem.id} />
      </CartItemStyles>
    );
  }
}

export default CartItem;
